package com.jbt.spring.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice 
public class ExceptionControllerAdvice { 
   @ExceptionHandler(Exception.class)
   @ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="This is a CUSTOM JBT MESSAGE User is not Authorized")
    public String exception(Exception e) { 
       return "error";
    }
 }