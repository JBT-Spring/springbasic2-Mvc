package com.jbt.spring.controller.exception;
import java.io.IOException;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

// YL Oct 2017
@Controller
@RequestMapping("/admin")
public class ExceptionController {

//	 http://localhost:8080//springbasicMvc/admin/get
//	 Will be captured by the UserNotLoggedInException external class
    @RequestMapping("/get")
    @ResponseStatus(HttpStatus.OK) 
    public @ResponseBody String get (HttpSession session, HttpServletRequest request) throws UserNotLoggedInException { 

        Object user = session.getAttribute("user");
        if (user == null) {
            throw new UserNotLoggedInException("user is null"); // Will be captured by the UserNotLoggedInException external class
        }
        return "test response " + user;
    }
 
//	 http://localhost:8080//springbasicMvc/admin/set
    @RequestMapping("/set")
    @ResponseStatus(HttpStatus.OK) 
    public @ResponseBody String set(HttpSession session, HttpServletRequest request) throws Exception {
    	session.setAttribute("user","Usr1");
    	return "SET!";
    }
    
//	 http://localhost:8080//springbasicMvc/admin/dataformat
//	 Will be captured by the handleDataFormatException method bellow
    @RequestMapping(value = "/dataformat", method=RequestMethod.GET)
    public String dataformat() throws DataFormatException {

      boolean throwException = true;
      if (throwException) {
        throw new DataFormatException("This is my Data Format Exception");
      }
      return "home";
    }
    
    @ExceptionHandler(DataFormatException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "55555 :-) JBT - Data Format Exception @@@ >>")
    public @ResponseBody DataFormatException handleDataFormatException(DataFormatException ex, HttpServletResponse response) throws IOException {
      System.out.println("Handlng DataFormatException - Catching: " + ex.getClass().getSimpleName());
      return new DataFormatException("This is a test " ); 
    }    
}