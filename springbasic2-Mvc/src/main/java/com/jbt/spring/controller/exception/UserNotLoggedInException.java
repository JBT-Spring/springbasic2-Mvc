package com.jbt.spring.controller.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN, reason="**** 88888888  :) Jbt Spring Demoes - User is not Authorized") //404
public class UserNotLoggedInException extends Exception {

    public UserNotLoggedInException (String message) {
        super(message);
    }
}