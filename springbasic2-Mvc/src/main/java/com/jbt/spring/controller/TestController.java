package com.jbt.spring.controller;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author yosilev
 * URLS:
 *     http://localhost:8080//springbasicMvc/actions/test
*/
@Controller
@RequestMapping(value = "/actions")
public class TestController {
	private static Logger logger = Logger.getLogger(TestController.class );

	@Autowired
	private ApplicationContext springApplicationContext;
	
	@Resource 
	private String helloStr;
	
	private final String SEP = "\n-------------------------------------\n";
	
	//-------- CTOR ---------------------------------------------------------------------------------------------------
	public TestController() {
		logger.debug(SEP + "TestController.CTOR() in");
	}
	//-----------------------------------------------------------------------------------------------------------------
	@PostConstruct
	public void init(){
		System.out.println("TestController.init() - helloStr:" + helloStr);
	}
	//-------- U T I L I T Y    M E T H O D S -------------------------------------------------------------------------
	// http://localhost:8080//springbasicMvc/actions/test
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public @ResponseBody String test() {
		logger.debug(SEP + "TestController.test() - ENTRY POINT");

		logger.debug("TestController.test() - END POINT\n" + SEP );
		return "TestController.test() - I'm OK ! ! helloStr:[" + helloStr +"]";
	}
	//-----------------------------------------------------------------------------------------------------------------
	// http://localhost:8080//springbasicMvc/actions/getEmployee
	@RequestMapping(value = "/getEmployee", method = RequestMethod.GET)
	public @ResponseBody Employee getEmployee() {
		logger.debug(SEP + "TestController.getEmployee() - ENTRY POINT");

		logger.debug("TestController.getEmployee() - END POINT\n" + SEP );
		return new Employee(111, "Joel", 31);
	}
	//-----------------------------------------------------------------------------------------------------------------
	// http://localhost:8080//springbasicMvc/actions/setEmployee
	@RequestMapping(value = "/setEmployee", method = RequestMethod.POST)
	public @ResponseBody Employee setEmployee(@RequestBody Employee emp, HttpServletRequest request) {
		logger.debug(SEP + "TestController.setEmployee() - ENTRY POINT emp=" + emp);

		logger.debug("TestController.setEmployee() - END POINT\n" + SEP );
		return new Employee(111, "Joel", 31);
	}
	//-----------------------------------------------------------------------------------------------------------------
	// http://localhost:8080//springbasicMvc/actions/action3/id/123/name/Dani
	@RequestMapping(value="/action3/id/{id}/name/{name}")
	public @ResponseBody String action3(@PathVariable("id") int id, @PathVariable("name") String name){
		return "action3 with id="+id + ", name=" + name;
	}	
}
