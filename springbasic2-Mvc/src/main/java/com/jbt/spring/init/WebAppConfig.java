package com.jbt.spring.init;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
//@EnableTransactionManagement
// OK @ComponentScan({"il.co.boj.jrulesweb"})
// OK @ComponentScan({"il.co.boj.jrules.memory.datagrid", "il.co.boj.jrules.service.impl", "il.co.boj.jrulesweb.controller"})

//OK latest -> @ComponentScan({"il.co.boj.jrules", "il.co.boj.jrulesweb.controller"})
@ComponentScan({ "com.jbt.spring"})
//@PropertySource("classpath:app_Oracle.properties")
                        
//@EnableJpaRepositories("com.jbt.spring")
public class WebAppConfig extends WebMvcConfigurerAdapter{

	private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(WebAppConfig.class);

	@Resource
	private Environment env;


	@Bean 
	public String helloBean(){
		return "Hello ! " + new Date();
	}
	
}